Foundation.Abide.defaults.validators['isEmptyName'] =
    function ($el, required, parent) {
        var length = document.getElementById('naam').value.length;
        if (length === 0) {
            $('#error1').html('Naam is verplicht.');
            return false;
        }
        return true;
    };


Foundation.Abide.defaults.validators['isEmptyBericht'] =
    function ($el, required, parent) {
        var length = document.getElementById('bericht').value.length;
        if (length === 0) {
            $('#error4').html('Bericht is verplicht.');
            return false;
        }
        return true;
    };


Foundation.Abide.defaults.validators['email'] =
    function ($el, required, parent) {
        // parameter 1 is jQuery selector

        var regexeml = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
        var val = document.getElementById('email').value;
        var length = document.getElementById('email').value.length;
        if (length === 0) {
            $('#error2').html('E-mail is verplicht.');
            return false;
        } else if (!val.match(regexeml)) {
            $('#error2').html('Ongeldig e-mail adres');
            return false;
        }
        return true;
    };
    $(document).foundation();
// // //FIXME: Fix abide, After invalid input errors stay even after correcting :(
// var abide = new Foundation.Abide($(document), options = {
//     live_validate: false, // validate the form as you go
//     validate_on_blur: true, // validate whenever you focus/blur on an input field
//     focus_on_invalid: true, // automatically bring the focus to an invalid input field
//     error_labels: true, // labels with a for="inputId" will recieve an `error` class
//     // the amount of time Abide will take before it validates the form (in ms).
//     // smaller time will result in faster validation
//     timeout: 10,
// });

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

$(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y > 400) {
        $('#scrollTopbtn').fadeIn('300');
    } else {
        $('#scrollTopbtn').fadeOut('300');
    }

});

$(function () {
    $(document).scroll(function () {
      var $nav = $(".navbar");
      $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });
  });

// $('#collapseOne').collapse({
//     hide: true
//   })