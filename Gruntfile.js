module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  const fileList = ['private/assets/scss/*.scss', 'private/assets/js/*.js','private/assets/scss/components*.scss'];
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),


    babel: {
      dev: {
        options: {
          sourceMap: true,
          minified: true,
          presets: ['env']
        },
        files: {
          'public/assets/js/main.min.js': 'private/assets/js/script.js',
        }

      },
      production: {
        options: {
          sourceMap: false,
          minified: true,
          presets: ['env']
        },
        files: {
          'public/assets/js/main.min.js': 'private/assets/js/script.js',
        }

      },
    },




    sass: {
      dev: {
        options: {
          style: 'compressed',
          sourcemap: 'auto',
          update: true
        },
        files: {
          'public/assets/css/style.min.css': 'private/assets/scss/main.scss' // 'destination': 'source' 
        }
      },
      production: {
        options: {
          style: 'compressed',
        },
        files: {
          'public/assets/css/style.min.css': 'private/assets/scss/main.scss' // 'destination': 'source' 
        }
      }
    },




    watch: {
      scripts: {
        files: fileList,
        tasks: ['concurrent'],
        options: {
          spawn: false
        },
      },
    },


    clean: {
      all: ['public/assets/js/*.min.js', 'public/assets/js/*.min.js.map', 'public/assets/css/*'],
      css: ['public/assets/css/*'],
      js: ['public/assets/js/*.min.js', 'public/assets/js/*.min.js.map']
    },

    concurrent: {
      target: {
          tasks: ['babel:dev','sass:dev'],
          options: {
              logConcurrentOutput: true
          }
      }
  },

  jshint: {
    ignore_warning: {
      options: {
        'esversion': 6,
        'reporter': require('jshint-stylish'),
        'force': true
      },
      src: 'private/assets/js/script.js',
      filter: 'isFile'
    }
  },



  });


  grunt.registerTask('dev', ['clean:all', 'jshint', 'babel:dev', 'sass:dev']);
  grunt.registerTask('production', ['clean:all', 'babel:production', 'sass:production']);

};