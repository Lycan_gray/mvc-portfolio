<?php
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('private/views/');
$twig = new Twig_Environment($loader, array(
   
));


echo $twig->render('layout.html.twig');